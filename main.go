package main

import (
	"fmt"
//	"html"
	"log"
	"net/http"
	"os"
	"strings"
	"encoding/json"
	"bytes"
	"io"
	"time"
)

// CONSTANTS:
const API_GBIF       = "http://api.gbif.org/v1/species/"
const API_COUNTRY    = "https://restcountries.eu/rest/v2/alpha/"
const API_OCCURRENCE = "http://api.gbif.org/v1/occurrence/"

// GLOBALE VARIABLE:
var startTime time.Time

// FUNCTION DEFINITIONS:
func speciesHandler(w http.ResponseWriter, r *http.Request) {

	parts := strings.Split(r.URL.Path, "/conservation/v1/species/")				// Manage the query after this path.
	query := parts[1]
	url1  := API_GBIF + query												    // Merge root-URL with query.
	url2  := API_GBIF + query + "/name"										    // To retieve 'year'.

	response1, err1 := http.Get(url1)											// GET requested URL.
		if err1 != nil {														// If error:
			fmt.Fprintf(w, "Bad request %s \n", err1)					    	// Output error message.
		}

	response2, err2 := http.Get(url2)											// GET request to find 'year'.
		if err2 != nil {
			fmt.Fprintf(w, "Bad request %s \n", err2)
		}

		var content1 Species													// Variable of type 'Species'.
		var content2 OccYear													// Variable of type 'OccYear'.

		err1 = json.NewDecoder(response1.Body).Decode(&content1)				// Decode the content of the request.
		err2 = json.NewDecoder(response2.Body).Decode(&content2)			    // Decode the content of the request.

		var mix SpecYear														// In order to list year for a speceies:
		mix.Species = content1													//  - save the content of Species in SpecYear struct
		mix.OccYear = content2													//  - save the content of OccYear in SpecYear struct

		var buffer = new(bytes.Buffer)										    // To output some JSON:
		encode := json.NewEncoder(buffer)										// - encode the buffer from memory
		encode.Encode(mix) 													    // - encode the content of mix into buffer

		w.Header().Set("content-type", "application/json")						// Output in this format.
		w.WriteHeader(http.StatusAccepted)
		io.Copy(w, buffer)														// Output content of buffer.

}

func countryHandler(w http.ResponseWriter, r *http.Request) {

	parts := strings.Split(r.URL.Path, "/conservation/v1/country/")				// Manage the query after this path.
	query := parts[1]
	url1  := API_COUNTRY + query												// Merge root-URL with query.
	url2  := API_OCCURRENCE + "search?country=" + query							// Merge root-URL with query.

	response1, err1 := http.Get(url1)									    	// Get country information.
		if err1 != nil {														// Error handling.
			fmt.Fprintf(w, "Bad request %s \n", err1)
		}
	response2, err2 := http.Get(url2)											// To get occurrences in this country.
		if err2 != nil {														// Error handling.
			fmt.Fprintf(w, "Bad request %s \n", err2)
	}
	
	var content1 Country														// Variable of type 'Country'.
	var content2 Occurrence														// Variable of type 'Occurrence'.

	err1 = json.NewDecoder(response1.Body).Decode(&content1)					// Decode the content of the request.
	err2 = json.NewDecoder(response2.Body).Decode(&content2)				    // Decode the content of the request.

	var mix Merged																// To list occurrences on a country:
	mix.Country    = content1													//  - save the content of Country in Merged struct
	mix.Occurrence = content2.Results											//  - save the content of Occurence in Merged struct

	var buffer = new(bytes.Buffer)												// To output some JSON:
	encode := json.NewEncoder(buffer)											// - encode the buffer from memory
	encode.Encode(mix)															// - encode the content of mix into buffer

	w.Header().Set("content-type", "application/json")							// Output in this format.
	w.WriteHeader(http.StatusAccepted)
	io.Copy(w, buffer)															// Output content of buffer.

}


func diagHandler(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "Welcome to the diagnostics page. \n\n")
	fmt.Fprintf(w, "To retrieve information about a species, try this path: https://rocky-plains-30580.herokuapp.com/conservation/v1/species/2491534 \n")
	fmt.Fprintf(w, "To retrieve information about species in a given country: https://rocky-plains-30580.herokuapp.com/conservation/v1/country/vn \n")
	fmt.Fprintf(w, "To set rate limits (max is 300), insert the following code after the country code: '&limit=50' \n")
	fmt.Fprintf(w, "Example of optional rate limit: https://rocky-plains-30580.herokuapp.com/conservation/v1/country/de&limit=50 \n")

	// SOURCE: https://golangcode.com/get-the-http-response-status-code/
	resp, err := http.Get("http://api.gbif.org/v1/")
    if err != nil {
        log.Fatal(err)
    }

    // Print the HTTP Status Code and Status Name
    fmt.Fprintf(w, "\n\nHTTP status response for GBIF:", resp.StatusCode, http.StatusText(resp.StatusCode))

    if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
        fmt.Fprintf(w, "\nHTTP Status is in the 200 range")
    } else {
        fmt.Fprintf(w, "\nGBIF API is currently unavailable. \n\n")
    }

		resp1, err1 := http.Get("https://restcountries.eu/")
	    if err1 != nil {
	        log.Fatal(err1)
	    }

	    // Print the HTTP Status Code and Status Name
	    fmt.Fprintf(w, "\n\nHTTP status response for REST-country:", resp1.StatusCode, http.StatusText(resp1.StatusCode))

	    if resp1.StatusCode >= 200 && resp1.StatusCode <= 299 {
	        fmt.Fprintf(w, "\nHTTP Status is in the 200 range")
	    } else {
	        fmt.Fprintf(w, "\nREST-Countries API is currently unavailable. \n\n")
	    }

			fmt.Fprintf(w, "\n\nVersion: V1")
			fmt.Fprintf(w, "\nApplication uptime: %s\n", uptime())

}

// In order to get the application uptime:
func uptime() time.Duration {
    return time.Since(startTime)
}

// In order to get the application uptime:
func init() {
    startTime = time.Now()
}

// STRUCTS:
type Species struct {

	Key 			int 		`json:"key"`
	Kingdom 		string		`json:"kingdom"`
	Phylum  		string		`json:"phylum"`
	Order			string		`json:"order"`
	Family		    string  	`json:"family"`
	Genus			string  	`json:"genus"`
	ScientificName	string  	`json:"scientificName"`
	CanonicalName	string  	`json:"canonicalName"`

}

type Country struct {

	Code string `json:"alpha2Code"`
	Name string `json:"name"`
	Flag string `json:"flag"`

}

type Occurrence struct {

	Results []Result  `json:"results"`

}

type Result struct {

  SpeciesName string  `json:"species"`
  SpeciesKey  int     `json:"speciesKey"`

}

type Merged struct {

	Country      Country
	Occurrence []Result

}

type OccYear struct {

	Year string `json:"year"`

}

type SpecYear struct {

	Species Species
	OccYear OccYear

}

// MAIN:
func main() {
	port := os.Getenv("PORT")
//	port = "8080"
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	http.HandleFunc("/conservation/v1/species/", speciesHandler)
  http.HandleFunc("/conservation/v1/country/", countryHandler)
	http.HandleFunc("/conservation/v1/diag/", diagHandler)
	http.HandleFunc("/conservation/v1/", diagHandler)							// Default if nothing stated.
	http.HandleFunc("/", diagHandler)											// Default if nothing stated.

	// log.Fatal(http.ListenAndServe(":"+port, nil))
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
