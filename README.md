The code works as intended and has a running instance in Heroku: https://rocky-plains-30580.herokuapp.com/
Open the provided Heroku-URL to find a guide for to how to use the application.

NOTE: 
The code could most certainly be more effective and written in fewer lines of code, but the restriction of performance here lies in the Internet connection anyways.
The output of the service would not appear any faster to the client if the code was more efficient.
The code is also human-readable in terms of variable and struct names.
Otherwise, it is worth to mention that this code does not manage duplicate occurrences in a given country.
I also register that the formatting in Git does not reflect my coding envorinment. Comments and structs are not alligned properly in Git.